import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  addFightEventListeners(firstFighter, secondFighter);

  return new Promise((resolve) => {
    document.addEventListener('finishFight',
        (event) => resolve(event.detail.winner));
  });
}

function addFightEventListeners(firstFighter, secondFighter) {
  let pressedKeys = new Set();

  const fullHealth1 = firstFighter.health;
  const fullHealth2 = secondFighter.health;

  let canDoCriticalHit1 = true;
  let canDoCriticalHit2 = true;

  document.addEventListener('keydown', function(event) {
    pressedKeys.add(event.code);

    if (pressedKeys.has(controls.PlayerOneAttack) &&
        !pressedKeys.has(controls.PlayerTwoBlock) &&
        !pressedKeys.has(controls.PlayerOneBlock)) {
      reduceHealth(firstFighter, secondFighter, fullHealth2, 'right');
    } else if (pressedKeys.has(controls.PlayerTwoAttack) &&
        !pressedKeys.has(controls.PlayerOneBlock) &&
        !pressedKeys.has(controls.PlayerTwoBlock)) {
      reduceHealth(secondFighter, firstFighter, fullHealth1, 'left');
    }

    outer: if (canDoCriticalHit1) {
      for (let code of controls.PlayerOneCriticalHitCombination) {
        if (!pressedKeys.has(code)) {
          break outer;
        }
      }
      reduceHealth(firstFighter, secondFighter, fullHealth2, 'right', true);
      canDoCriticalHit1 = false;
      setTimeout(() => canDoCriticalHit1 = true, 10000);
      pressedKeys.clear();
    }

    outer: if (canDoCriticalHit2) {
      for (let code of controls.PlayerTwoCriticalHitCombination) {
        if (!pressedKeys.has(code)) {
          break outer;
        }
      }
      reduceHealth(secondFighter, firstFighter, fullHealth1, 'left', true);
      canDoCriticalHit2 = false;
      setTimeout(() => canDoCriticalHit2 = true, 10000);
      pressedKeys.clear();
    }
  });

  document.addEventListener('keyup', (event) => {
    pressedKeys.delete(event.code);
  });

}

function reduceHealth(attacker, defender, fullHealth, position, criticalHit = false) {
  let damage;
  if (criticalHit) {
    damage = getCriticalHitDamage(attacker);
  } else {
    damage = getDamage(attacker, defender);
  }

  defender.health -= damage;
  reduceHealthBar(defender, fullHealth, position);

  if (defender.health <= 0) {
    defender.health = 0;
    reduceHealthBar(defender, fullHealth, position);
    finishFight(attacker);
  }
}

function reduceHealthBar(fighter, fullHealth, position) {
  let healthBar = document.getElementById(`${position}-fighter-indicator`);
  let newWidth = (fighter.health * 100) / fullHealth;
  healthBar.style.width = newWidth + '%';
}

function finishFight(winner) {
  document.dispatchEvent(new CustomEvent('finishFight', {
    detail: {winner}
  }));
}

function getCriticalHitDamage(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {

  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  if (damage < 0) {
    return 0;
  }

  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;

  return defense * dodgeChance;
}
