import {showModal} from "./modal";
import {createFighterImage} from "../fighterPreview";

export function showWinnerModal(fighter) {
    const args = {
        title: `Congratulations, ${fighter.name}!`,
        bodyElement: createFighterImage(fighter)
    }

    showModal(args);
}
