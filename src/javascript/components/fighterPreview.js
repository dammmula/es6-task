import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return '';
  }

  try {
    const imageElement = createFighterImage(fighter);
    const name = createInfoElement(fighter, 'name');
    const health = createInfoElement(fighter, 'health');
    const defense = createInfoElement(fighter, 'defense');
    const attack= createInfoElement(fighter, 'attack');

    fighterElement.classList.add('fighter-preview-info');
    fighterElement.append(imageElement, name, health, defense, attack);

  } catch (error) {
    throw error;
  }

  return fighterElement;
}

function createInfoElement(fighter, field) {
  const element = createElement({tagName: 'span', className: ''});
  element.innerText = `${field}: ${fighter[field]}`;

  return element;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
